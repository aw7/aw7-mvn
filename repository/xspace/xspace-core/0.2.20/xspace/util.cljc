(ns xspace.util)

(defn- cartesian-product
  "from clojure.math.combinatorics"
  [& seqs]
  (let [v-original-seqs (vec seqs)
        step
        (fn step [v-seqs]
          (let [increment
                (fn [v-seqs]
                  (loop [i (dec (count v-seqs)), v-seqs v-seqs]
                    (if (= i -1) nil
                      (if-let [rst (next (v-seqs i))]
                        (assoc v-seqs i rst)
                        (recur (dec i) (assoc v-seqs i (v-original-seqs i)))))))]
            (when v-seqs
              (cons (map first v-seqs)
                    (lazy-seq (step (increment v-seqs)))))))]
    (when (every? seq seqs)
      (lazy-seq (step v-original-seqs)))))

(defn bnd-map-to-args-seq [bnd-map]
  (->> bnd-map
       (map (fn [[k v]]
              (map vector (repeat k) v)))
       (apply cartesian-product)
       (map #(into {} %))))
