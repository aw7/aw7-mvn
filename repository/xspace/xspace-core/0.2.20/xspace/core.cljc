(ns xspace.core
  #?(:cljs (:require-macros xspace.core))
 (:require [xspace.util :as util]))

(defn proc-vals [f-proc args-seq]
  (->> (partition 2 args-seq)
       (into {}
         (map (fn [[k v]] [k (f-proc k v)])))))

(defrecord XDef    [c args-seq])
(defrecord XFor    [c bnd-map xx])
(defrecord XDeriv  [c args-seq])
(defrecord XFnAppl [c args-seq])
(defrecord XX      [c xx])
(defrecord Return  [value c-ctx args-ctx out thunk chan])
(defrecord XFnCtx  [x c args])

(def x:=?  (partial instance? XDef))
(def xfor? (partial instance? XFor))
(def x:>?  (partial instance? XDeriv))
(def x->?  (partial instance? XFnAppl))
(def xx?   (partial instance? XX))
(def ret?  (partial instance? Return))
(def xctx? (partial instance? XFnCtx))

(defn- parse-args
  [[a0 & arest :as aa]]
  (cond (string? a0) [{:title a0} arest]
        (x:=?    a0) (let [m (->> (:args-seq a0)
                                  (partition 2)
                                  (into {} (map vec)))]
                       [{:= m} arest])
        (map?    a0) [a0 arest]

        :else        [nil aa]))

(defn- ctor-factory [f]
  (fn [& args] (let [[h tail] (parse-args args)] (f h tail))))

(def x:=  (ctor-factory ->XDef))
(def xfor (fn [bnd-map & bb] (->XFor nil bnd-map bb)))
(def x:>  (ctor-factory ->XDeriv))
(def x->  (ctor-factory ->XFnAppl))
(def xx   (ctor-factory ->XX))
(defn return [& {:as args}] (map->Return args))
(def  xctx ->XFnCtx)


(def get-args-ctx #(when (ret? %) (:args-ctx %)))
(def get-conf-ctx #(when (ret? %) (:c-ctx    %)))

(def get-value    #(cond-> % (ret? %) :value))
(def get-thunk    #(cond-> % (ret? %) :thunk))
(def get-out      #(cond-> % (ret? %) :out))

(def set-attr     #(-> %1 (cond->> (not (ret? %)) (return :value))
                          (assoc %2 %3)))

(def args--defaults
  {:-> (fn [ctx c arg]
         (if (fn? arg)
           (arg (get (:args ctx) :->))
           arg))})

;; default
(def derive--merge-ctx
  {:c    (fn [_ c] c) :c-ctx    merge
   :args (fn [_ a] a) :args-ctx merge})

(def derive--merge-active
  {:c    merge :c-ctx    (fn [_ c'] c')
   :args merge :args-ctx (fn [_ a'] a')})

(def fns--defaults
  {:x?   (fn [c-ctx x] (-> x :c :disabled? not))
   :x:=  (fn [ctx c args] nil)
   :x:>  (fn [ctx c args] nil)
   :x->  (fn [ctx c args] (return :thunk (fn []
                                           (assert
                                             (->> (merge (:args ctx) args)
                                                  vals
                                                  (apply =))))))
   :xx   (fn [ctx c frec] (return :thunk (fn [] (frec))))
   :xfor (fn [ctx c frec _frec-ui] (return :thunk (fn [] (frec))))})

(defn- apply-arg-transformer [c-fns conf-ctx conf-loc args-ctx xrec k v]
  (if-let [f (get-in c-fns [:args k]
                     (get args--defaults k))]
    (f (xctx xrec conf-ctx args-ctx) conf-loc v)
    v))

(defn get-fn [c-fns fn-k]
  (let [f (get-in c-fns [:fns fn-k]
                        (get fns--defaults fn-k))]
    (assert f (str "no fn for: " (pr-str fn-k)))
    f))

(defn- derive-next [c-fns fn-k v1 v2]
  (let [f (get-in c-fns [:derive fn-k]
                  (get derive--merge-ctx fn-k))]
    (assert f (str "no fn for: " (pr-str fn-k)))
    (f v1 v2)))

(declare traverse-xspace apply-x-fn)

(defn- apply-xx-fn
  [c-fns conf-ctx args-ctx {:as xrec, conf-loc :c}]
  {:pre [(xx? xrec)]}
  (let [f         (get-fn c-fns :xx)
        args-ctx' (if-let [x:=map (:= conf-loc)]
                    (get-args-ctx
                     (apply-x-fn c-fns conf-ctx args-ctx
                                 (apply x:= (apply concat x:=map))))
                    args-ctx)
        conf-act  (derive-next c-fns :c     conf-ctx conf-loc)
        conf-ctx' (derive-next c-fns :c-ctx conf-ctx conf-act)
        f-recur   (fn []
                    ((or (:traverse-xspace c-fns)
                         traverse-xspace)  c-fns conf-ctx' args-ctx' (:xx xrec)))]

    (f (xctx xrec conf-ctx' args-ctx') conf-act f-recur)))

(defn- apply-xfor-fn
  [c-fns conf-ctx args-ctx {:as xrec :keys [bnd-map], conf-loc :c}]
  {:pre [(xfor? xrec)]}
  (let [f            (get-fn c-fns :xfor)
        conf-act     (derive-next c-fns :c     conf-ctx conf-loc)
        conf-ctx'    (derive-next c-fns :c-ctx conf-ctx conf-act)
        f-recur-all  (fn thunk []
                       (doall
                         (for [x:=map (util/bnd-map-to-args-seq bnd-map)]
                           (let [out       (apply-x-fn c-fns conf-ctx args-ctx
                                                       (apply x:= (apply concat x:=map)))
                                 args-ctx* (get-args-ctx out)]
                             (traverse-xspace c-fns conf-ctx' args-ctx* (:xx xrec))))))
        f-recur-flat (fn thunk []
                       (traverse-xspace c-fns conf-ctx' args-ctx (:xx xrec)))]
    (f (xctx xrec conf-ctx' args-ctx) conf-act f-recur-all f-recur-flat)))

(defn- get-fn-key [xrec]
  (condp #(%1 %2) xrec
    x:=?  :x:=
    xfor? :xfor
    x:>?  :x:>
    xx?   :xx
    x->?  :x->))

(defn- apply-x-fn
  [c-fns conf-ctx args-ctx {:as xrec, conf-loc :c, :keys [args-seq]}]
  {:pre [((some-fn x->? x:=? x:>?) xrec)]}
  (let [f        (->> (get-fn-key xrec)
                      (get-fn c-fns))
        proc-f   (cond
                   (x:>? xrec) (fn [_k v]
                                 (get-out
                                  (apply-x-fn c-fns conf-ctx args-ctx
                                              (map->XFnAppl
                                               {:c     conf-loc
                                                :args-seq (->> v (apply concat))}))))
                   :else       (fn [k v]
                                 (apply-arg-transformer c-fns conf-ctx conf-loc args-ctx xrec k v)))
        args-loc (proc-vals proc-f args-seq)
        args-act (derive-next c-fns :args args-ctx args-loc)
        conf-act (derive-next c-fns :c    conf-ctx conf-loc)]
    (-> (f (xctx xrec conf-ctx args-ctx) conf-act args-act)
        (cond-> (or (x:=? xrec)
                    (x:>? xrec))
                (-> (set-attr :args-ctx (derive-next c-fns :args-ctx args-ctx args-act))
                    (set-attr :c-ctx    (derive-next c-fns :c-ctx    conf-ctx conf-act)))))))

(defn- opt-exec! [out]
  (when (ret? out)
    (some-> (get-thunk out) (apply []))))

(defn dispatch-f [xrec]
  (cond
    (xx?   xrec) apply-xx-fn
    (xfor? xrec) apply-xfor-fn
    :else        apply-x-fn))

(defn x-active? [x? conf-ctx {:as xrec, conf-loc :c}]
  (and (x? conf-ctx xrec)
       (if-let [cljc (or (and (not ;; don't filter the x:= that sets :cljc
                                   (x:=? xrec))
                              (:cljc conf-loc))
                         (:cljc conf-ctx))]
         (= cljc #?(:clj :clj, :cljs :cljs))
         true)))

(defn traverse-xspace
  ([c-fns xx-seq] (traverse-xspace c-fns nil nil xx-seq))
  ([c-fns conf-ctx args-ctx xx-seq]
   (loop [[xrec
           & xx-tail] xx-seq
          val-acc     nil
          conf-ctx    conf-ctx
          args-ctx    args-ctx]
     (cond
       (not xrec)
       (reverse val-acc)

       (not (x-active? (get-fn c-fns :x?) conf-ctx xrec))
       (recur xx-tail val-acc conf-ctx args-ctx)

       :else
       (let [out ((dispatch-f xrec) c-fns conf-ctx args-ctx xrec)]
         (opt-exec! out)
         (recur xx-tail
                (-> val-acc (conj (get-value out)))
                (or (:c-ctx    out) conf-ctx)
                (or (:args-ctx out) args-ctx)))))))

(defn count-x-> [xs]
  (->> xs
       (traverse-xspace {:fns {:xfor (fn [_ _ f _] (f))
                               :xx   (fn [_ _ f]   (f))
                               :x->  (fn [_ _ _]   1)}})
       flatten
       (remove nil?)
       (apply +)))
